import request from "supertest";
import app from "../app";

describe("GET /ping", () => {
    it("Should get the url", async () => {
        const response = await request(app).get("/ping").send();
        expect(response.status).toEqual(200);
        expect(response.text).toBe("pong");
    });
});

describe("GET /tasks", () => {
    it("Shoudl response with array", async () => {
        const response = await request(app).get("/tasks").send();
        expect(response.status).toEqual(200);
        expect(response.body).toBeInstanceOf(Array);
    });
});

describe("POST /tasks", () => {
    it("Shoudl response with tasks", async () => {
        const response = await request(app).post("/tasks").send({
            info: "hola",
            name: "queso",
        });

        // console.log(response.body);
        expect(response.status).toEqual(201);
        expect(response.headers["content-type"]).toEqual(
            expect.stringContaining("json")
        );
        expect(response.body.info).toBe("hola");
        expect(response.body.name).toBe("queso");
    });
});
