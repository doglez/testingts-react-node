import express from "express";

const routes = express.Router();

routes.get("/ping", (_req, res, _next) => {
    return res.send("pong");
});

routes.get("/tasks", (_req, res, _next) => {
    return res.send([]);
});

routes.post("/tasks", async (req, res, _next) => {
    const info = await req.body;
    return res.status(201).json(info);
});

export default routes;
